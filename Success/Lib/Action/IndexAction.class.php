<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    public function index(){
    	 // $this->redirect(string url, array params, int delay, string msg) 
    	$this->redirect('redirect','',60,'页面跳转中~');
    }

    public function success(){
    	// 省略部分其他代码
    	$flag = true;
	    if($flag){
	        // 页面跳转目标地址
	        $this->assign("waitSecond",60);
	        $this->success("成功信息");
	    }else{
	    	$this->assign("waitSecond",60);
	        $this->error("错误信息");
	    }
    }

    public function redirect(){
    	echo "this is redirect function !";
    }
}