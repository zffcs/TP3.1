<?php
class OtherAction extends Action{
	function index(){
		header("Content-type:text/html;charset=utf8");
		$food = array();
		$food[0]['fruits'][0]['fruits1'] = '苹果1';
		$food[0]['fruits'][0]['fruits2'] = '桔子1';
		$food[0]['fruits'][0]['fruits3'] = '香蕉1';
		$food[0]['vegetables'] = '白菜1';
		$food[1]['fruits'][1]['fruits1'] = '苹果2';
		$food[1]['fruits'][1]['fruits2'] = '桔子2';
		$food[1]['fruits'][1]['fruits3'] = '香蕉2';
		$food[1]['vegetables'] = '白菜2';
		$food[2]['fruits'][1]['fruits1'] = '苹果3';
		$food[2]['fruits'][1]['fruits2'] = '桔子3';
		$food[2]['fruits'][1]['fruits3'] = '香蕉3';
		$food[2]['vegetables'] = '白菜3';
		// print_r($food);
		$this->assign( "food", $food );
		$this->display();
	}
}

// Array ( 
// 	[0] => Array ( 
// 		[fruits] => Array ( 
// 			[0] => Array ( 
// 				[fruits1] => 苹果1 
// 				[fruits2] => 桔子1 
// 				[fruits3] => 香蕉1 
// 				) 
// 			) 
// 		[vegetables] => 白菜1 
// 		) 
// 	[1] => Array ( 
// 		[fruits] => Array (
// 		 [1] => Array ( 
// 		 	[fruits1] => 苹果2 
// 		 	[fruits2] => 桔子2 
// 		 	[fruits3] => 香蕉2 
// 		 	) 
// 		 ) 
// 		[vegetables] => 白菜2 
// 		) 
// 	) 