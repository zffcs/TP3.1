<?php

class IndexAction extends Action {

    public function index() {
        $Photo  =   M('Photo');
        $data   =   $Photo->order('create_time desc')->find();
        $this->assign('data', $data);
        $this->display();
    }

    public function upload() {
        if (!empty($_FILES)) {
            //如果有文件上传 上传附件
            $this->_upload();
        }
    }

    // 文件上传
    protected function _upload() {
        import('@.ORG.UploadFile');
        //导入上传类
        $upload = new UploadFile();
        //设置上传文件大小
        $upload->maxSize            = 3292200;
        //设置上传文件类型
        $upload->allowExts          = explode(',', 'jpg,gif,png,jpeg');
        //设置附件上传目录
        $upload->savePath           = APP_PATH.'Uploads/';
        //设置需要生成缩略图，仅对图像文件有效
        $upload->thumb              = true;
        // 设置引用图片类库包路径
        $upload->imageClassPath     = '@.ORG.Image';
        //设置需要生成缩略图的文件后缀
        $upload->thumbPrefix        = 'm_,s_';  //生产2张缩略图
        //设置缩略图最大宽度
        $upload->thumbMaxWidth      = '400,100';
        //设置缩略图最大高度
        $upload->thumbMaxHeight     = '400,100';
        //设置上传文件规则
        $upload->saveRule           = 'uniqid';
        //删除原图
        $upload->thumbRemoveOrigin  = true;
        if (!$upload->upload()) {
            //捕获上传异常
            $this->error($upload->getErrorMsg());
        } else {
            //取得成功上传的文件信息
            $uploadList = $upload->getUploadFileInfo();
            $model = M('Photo');
            dump($uploadList);
            import('@.ORG.Image');
            //给m_缩略图添加水印, Image::water('原文件名','水印图片地址')
            foreach ($uploadList as $key => $value) {
                echo $value['savepath'] . 'm_' . $value['savename']."<br/>";
                Image::water($value['savepath'] . 'm_' . $value['savename'],'./logo.png');
                $data['image'] = $value['savename'];
                $data['create_time']    = NOW_TIME;
                $list   = $model->add($data);
                if ($list === false) {
                    $this->error('上传图片失败!');
                    break;
                }
            }
        }
    }
}