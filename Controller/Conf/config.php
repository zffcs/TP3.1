<?php
return array(
	//'配置项'=>'配置值'
    'SHOW_PAGE_TRACE' =>true, // 显示页面Trace信息
    'URL_HTML_SUFFIX'=>'shtml|html|htm',
    'DB_DSN' => 'mysql://root:111111@localhost:3306/heavendb', // 添加数据库配置信息
    'DB_PREFIX' => 'think_', // 数据库表前缀

    'DEFAULT_CHARSET'      => 'utf-8',     // 默认输出编码
    /* Cookie设置 */
	'COOKIE_EXPIRE'        => 3600,            // Coodie有效期
	'COOKIE_DOMAIN'        => '.5idev.com',    // Cookie有效域名
	'COOKIE_PATH'          => '/',             // Cookie路径
	'COOKIE_PREFIX'        => '',              // Cookie前缀

	/* 网站设置 */
	'SITE_NAME'          => '我爱开发网23333',	    // 站点名字
	'DOMAIN_NAME'        => '5idev.com',	    // 域名
	'NO_ARTICLE_VIEW'    => '抱歉：您请求的文章不存在，系统已记录该错误。请继续访问本站其他内容。',
);
?>