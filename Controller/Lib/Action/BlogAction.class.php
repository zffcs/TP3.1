<?php
class BlogAction extends Action{
    /*
     * 因为在执行read操作方法的时候，id参数是必须传入参数的，但是方法无法从URL地址中获取正确的id参数信息。由于我们不能相信用户的任何输入，因此建议你给read方法的id参数添加默认值
     */
    public function read($id=0){
        echo 'id='.$id;
    }
  
    /*
     * URL中的参数顺序和操作方法中的参数顺序都可以随意调整，关键是确保参数名称一致即可
     */
    public function archive($year='2012',$month='01'){
        echo 'year='.$year.'&month='.$month;
    }
    
    /*
     * 空操作是指系统在找不到指定的操作方法的时候，会定位到空操作（_empty）方法来执行，利用这个机制，我们可以实现错误页面和一些URL的优化。
     */
    public function _empty($name){
       //把所有城市的操作解析到city方法
       $this->city($name);
    }
            
    //注意 city方法 是 protected 方法
    protected function city($name){
        //和$name这个城市相关的处理
        header("Content-type:text/html;charset=utf-8");
        echo '当前城市' . $name;
     }
}