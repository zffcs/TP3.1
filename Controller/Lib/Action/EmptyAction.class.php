<?php
class EmptyAction extends Action{
    /*
     * 空模块和空操作还可以同时使用，用以完成更加复杂的操作。
     
     */
    public function index(){
        //根据当前模块名来判断要执行那个城市的操作
        $cityName = MODULE_NAME;
        $this->city($cityName);
    }
   //注意 city方法 本身是 protected 方法
   protected function city($name){
       //和$name这个城市相关的处理
       header("Content-type:text/html;charset=utf-8");
       echo '当前城市' . $name;
    }
    
    public function _empty($name){
        //把所有城市的操作解析到city方法
        $this->city($name);
    }
    
}