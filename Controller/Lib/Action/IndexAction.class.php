<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    public function index(){
        
        echo "this is index function !<br/>";
        $data['status'] = 11;
        $data['info'] = 'info';
        $data['data'] = 'data';
        $this->ajaxReturn($data);
//         $this->json();
// 	$this->show('<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} body{ background: #fff; font-family: "微软雅黑"; color: #333;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.8em; font-size: 36px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p>欢迎使用 <b>ThinkPHP</b>！</p></div><script type="text/javascript" src="http://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script>','utf-8');
    }
    
    //前置操作方法
    public function _before_index(){
        echo 'before<br/>';
    }
    //后置操作方法
    public function _after_index(){
        echo 'after<br/>';
    }
    
    public function _empty($name){
        //把所有城市的操作解析到city方法
        $this->index($name);
    }
    
    public function forward(){
        $User = new UserModel(); //实例化User对象
        $result = $User->show();
        if(false){
//             //设置成功后跳转页面的地址，默认的返回页面是$_SERVER['HTTP_REFERER']
//             $this->success('新增成功', 'Index/index');
            //重定向到New模块的Category操作
            $this->redirect('Index/index', 
            array('cate_id' => 2), 
            2, 
            'loading...');
        } else {
            //错误页面的默认跳转页面是返回前一页，通常不需要设置
//             $this->error('新增失败');
        }
    }
    
    public function json(){
        $data['status'] = 11;
        $data['info'] = 'info';
        $data['data'] = 'data';
        $this->ajaxReturn($data);
//         $this->assign('data',$data);
//         $this->display('Index:index');
    }
}