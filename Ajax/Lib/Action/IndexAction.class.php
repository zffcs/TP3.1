<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    public function index(){
        $Form = M("Form");
        // 按照id排序显示前5条记录
        $list = $Form->order('id desc')->limit(20)->select();
        $this->list =   $list;
        $this->display();
    }
    
//     // 处理表单数据
//     public function insert() {
//         $Form = D("Form");
//         if ($Form->create()) {
//             if (false !== $Form->add()) {
//                 $this->success('数据添加成功！');
//             } else {
//                 $this->error('数据写入错误');
//             }
//         } else {
//             // 字段验证错误
//             $this->error($Form->getError());
//         }
//     }

    //采用AjaxReturn方法返回JSON数据到浏览器
    public function insert() {
        $Form = D("Form");
        if ($vo = $Form->create()) {
            if (false !== $Form->add()) {
                $vo['create_time'] = date('Y-m-d H:i:s', $vo['create_time']);
                $vo['content'] = nl2br($vo['content']);
                $this->ajaxReturn($vo, '表单数据保存成功！', 1);
            } else {
                $this->error('数据写入错误！');
            }
        } else {
            $this->error($Form->getError());
        }
    }
    
    public function checkTitle($title='') {
        if (!empty($title)) {
            $Form = M("Form");
            if ($Form->getByTitle($title)) {
                $this->error('标题已经存在');
            } else {
                $this->success('标题可以使用!');
            }
        } else {
            $this->error('标题必须');
        }
    }
}