<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {
    public function index(){
        echo "this is indexaction index function !<br/>";
        //1
        $this->show();
        //2
        /*
         * 如果要使用其他模块内的多个方法，那么建议使用 A 方法，通过对象的方式调用该模块的不同方法，避免多次实例化对象
         */
        $sayhello = A('Other');
        $sayhello->sayhello();
        //3
        /*
         * 如果只需要使用其他模块内的其中一个方法，那么无疑 R 方法是最简洁的。 
         */
        R('Other/sayhello');
    }
    
    protected function show(){
        echo "this is indexaction show function !<br/>";
    }
    
   
}