<?php
/*
 * 每个项目都有一个独立的配置文件（位于项目目录的Conf/config.php），配置文件的定义格式均采用PHP返回数组的方式
 * 配置值可以支持包括字符串、数字、布尔值和数组在内的数据，通常我们建议配置参数均使用大写定义。如果有需要，我们还可以为项目定义其他的配置文件。
 */
return array(
	//'配置项'=>'配置值'
	'SHOW_PAGE_TRACE' =>true, // 显示页面Trace信息
	'URL_HTML_SUFFIX'=>'shtml|html|htm',
    'URL_PATHINFO_DEPR'=>'-', // 更改PATHINFO参数分隔符
    /*
     * 使用DB_DSN方式定义可以简化配置参数，DSN参数格式为：
数据库类型://用户名:密码@数据库地址:数据库端口/数据库名
如果两种配置参数同时存在的话，DB_DSN配置参数优先。
     */
    'DB_DSN' => 'mysql://root:111111@localhost:3306/heavendb', // 添加数据库配置信息
    'DB_PREFIX' => 'think_', // 数据库表前缀
);
?>