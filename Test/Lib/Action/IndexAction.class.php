<?php
// 本类由系统自动生成，仅供测试用途
/*
 * 需要为每个模块定义一个控制器类，控制器类的命名规范是：
模块名+Action.class.php （模块名采用驼峰法并且首字母大写）
系统的默认模块是Index，对应的控制器就是项目目录下面的Lib/Action/IndexAction.class.php，类名和文件名一致。
 */
class IndexAction extends Action {
    /*
     * 控制器必须继承Action类，一个模块可以包括多个操作方法。如果你的操作方法是protected或者private类型的话，是无法直接通过URL访问到该操作的。
     */
    public function index(){
	// $this->show('<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} body{ background: #fff; font-family: "微软雅黑"; color: #333;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.8em; font-size: 36px }</style><div style="padding: 24px 48px;"> <h1>:)</h1><p>欢迎使用 <b>ThinkPHP</b>！</p></div><script type="text/javascript" src="http://tajs.qq.com/stats?sId=9347272" charset="UTF-8"></script>','utf-8');

        /*
         * 这里用到了M函数，是ThinkPHP内置的实例化模型的方法，而且用M方法实例化模型不需要创建对应的模型类，你可以理解为M方法是直接在操作底层的Model类，而Model类具备基本的CURD操作方法。
         */
        $data = M('Data');
        $this->data = $data->select();
    	$this->display('Index:index');
    }
    public function login() {

    	echo "this is login function";
    	echo "<br/>";
    	//PHP内置函数
    	echo $_GET['username'];
    	echo "<br/>";
    	echo $_GET['password'];
    	//默认的获取参数的方式
    	// echo I('get.username');
    	// echo "<br/>";
    	// echo I('get.password');
    	//含有默认值,如果不存在
    	echo "<br/>";
    	echo I('get.username','我没有名字','htmlspecialchars');
    	echo "<br/>";
    	echo I('get.password','我没有密码','htmlspecialchars');
    	//获取整个数组
    	dump(I('get.'));
    	dump(I('post.'));
    	//自动判断类型获取参数
    	echo "<br/>";
    	echo I('username');
    	echo "<br/>";
    	echo I('password');
    	$this->display('Index:login');
    }
    public function sayhello(){
    	echo "this is test model index action sathello function";
    }
}