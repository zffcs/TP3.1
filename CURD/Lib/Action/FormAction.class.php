<?php
class FormAction extends Action{
    /*
     * 如果使用D函数实例化模型类，一般需要对应一个数据模型类，而且create方法会自动把表单提交的数据进行自动验证和自动完成（如果有定义的话），如果自动验证失败，就可以通过模型的getError方法获取验证提示信息，如果验证通过，就表示数据对象已经成功创建，但目前只是保存在内存中，直到我们调用add方法写入数据到数据库。这样就完成了一个完整的Create操作，所以可以看到ThinkPHP在创建数据的过程中使用了两步：
第一步，create方法创建数据对象，
第二步，使用add方法把当前的数据对象写入数据库。
当然，你完全可以跨过第一步，直接进行第二步，但是这样的预处理有几个优势：
1、无论表单有多复杂，create方法都可以用一行代码轻松创建数据对象；
2、在写入数据之前，可以对数据进行验证和补充；
     */
    public function insert(){
        /*
         * 我们在insert操作方法中用了D函数，和M函数不同，D函数需要有对应的模型类，
         */
        $Form   =   D('Form');
        if($Form->create()) {
            $result =   $Form->add();
            if($result) {
                $this->success('操作成功！');
            }else{
                $this->error('写入错误！');
            }
        }else{
            $this->error($Form->getError());
        }

/*
 * 如果你的数据完全是内部操作写入而不是通过表单的话（也就是说可以充分信任数据的安全），那么可以直接使用add方法
 * 对象方式操作的时候，add方法无需传入数据，会自动识别当前的数据对象赋值
 */
//         $Form   =   D('Form');
//         $data['title']  =   'hello';
//         $data['content']    =   '打个招呼';
//         $Form->add($data);
    }
    
    /*
     * 这里我们来通过find方法获取一个单一数据
     * read操作方法有一个参数$id，表示我们可以接受URL里面的id变量（后面我们会在变量章节详细描述。这里之所以用M方法而没有用D方法，是因为find方法是基础模型类Model中的方法，所以没有必要浪费开销去实例化FormModel类（即使已经定义了FormModel类）。我们通常采用find方法读取某个数据，这里使用了AR模式来操作，所以没有传入查询条件，find($id) 表示读取主键为$id值的数据
     */
    public function read($id=4){
        G('begin');
        $Form   =   M('Form');
        // 读取数据
        $data =   $Form->find($id);
        trace($data,'create data');
        /*
         * 上面的用法表示获取id值为3的数据的title字段值。其实getField方法有很多用法，但是获取某个字段的值是getField方法最常规的用法。
         */
//         $title = $Form->where($id)->getField('title');
//         dump($title);
        if($data) {
            $this->data =   $data;// 模板变量赋值
        }else{
            $this->error('数据错误');
        }
        G('end');
        echo G('begin','end',6).'s';
        echo "<br/>";
        echo G('begin','end','m').'kb';
        echo "<br/>";
        $content = $this->fetch();
        $this->show($content);
        echo "<br/>";
        echo var_dump($_ENV);
        $this->display();
    }
    
    public function edit($id=0){
        $Form   =   M('Form');
        $this->vo   =   $Form->find($id);
        $this->display();
    }
    /*
     * 数据的更新操作在ThinkPHP使用save方法，可以看到，我们同样可以使用create方法创建表单提交的数据，而save方法则会自动把当前的数据对象更新到数据库，而更新的条件其实就是表的主键，这就是我们在编辑页面要把主键的值作为隐藏字段一起提交的原因。
     */
    public function update(){
        $Form   =   D('Form');
        if($Form->create()) {
            $result =   $Form->save();
            if($result) {
                $this->success('操作成功！');
            }else{
                $this->error('写入错误！');
            }
        }else{
            $this->error($Form->getError());
        }
//         $Form->where('id=1')->setField('title','ThinkPHP');
        /*
         * 如果更新操作不依赖表单的提交的话，就可以写成：
         * 数据对象赋值的方式，save方法无需传入数据，会自动识别。
         */
//         $Form = M("Form");
//         // 要修改的数据对象属性赋值
//         $Form->title = 'ThinkPHP';
//         $Form->content = 'ThinkPHP3.1版本发布';
//         echo $Form->where('id=1')->save(); // 根据条件保存修改的数据

        //对于统计字段，系统还提供了更加方便的setInc和setDec方法。
//         $User = M("Form"); // 实例化User对象
//         $User->where('id=1')->setInc('create_time',3); // 用户的积分加3
//         $User->where('id=5')->setInc('create_time'); // 用户的积分加1
//         $User->where('id=1')->setDec('create_time',5); // 用户的积分减5
//         $User->where('id=5')->setDec('create_time'); // 用户的积分减1
    }
    
    public function delete() {
        $User = M("Form"); // 实例化User对象
        echo $User->where('id=1')->delete(); // 删除id为5的用户数据
//         echo $User->delete('2,3'); // 删除主键为1,2和5的用户数据
//         $User->where('status=0')->delete(); // 删除所有状态为0的用户数据
    }
    
    public function index(){
        //当select方法的参数为false的时候，表示不进行查询只是返回构建SQL，例如： 
        $model = M("Form");
        echo $subQuery = $model->select(false);
        echo "<br/>";
        echo $subQuery = $model->buildSql();
        dump($model->table($subQuery.' a')->select());
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}